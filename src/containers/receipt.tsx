import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createStructuredSelector } from "reselect";
import { useHistory, useLocation } from "react-router-dom";
import { ImgAds } from "../components/Ads";
import {
  ReceiptPage,
  ReceiptInfo,
  IngredientsRow,
  InstructionRow,
  BackArrow,
} from "./styles/receipt";

import Skeleton from "react-loading-skeleton";

import getMainDataAction from "../__data__/actions/recipes";
import {
  getRecipes,
  getMainDataLoading,
  getError,
} from "@main/__data__/selectors/mainData";
import { Img } from "@main/components/MealCard/styled";

const Receipt = ({ getMainData, recipes, isLoading, error }) => {
  const [recipe, onLoadRecipe] = React.useState<any>({});
  const location = useLocation().pathname;

  React.useEffect(() => {
    getMainData();
  }, []);

  React.useEffect(() => {
    if (!isLoading && !!recipes) {
      onLoadRecipe(
        recipes.result?.filter(
          (item) => item._id === location?.split("/")[2]
        )[0]
      );
    }
  }, [recipes]);

  return (
    <ReceiptPage>
      <h1>{recipe.name}</h1>
      <ReceiptInfo>
        <span>{recipe.portion} portions</span>
        <span>{recipe.time} minuts</span>
        <span>{recipe.kcal} kcal</span>
      </ReceiptInfo>
      <p>{recipe.description}</p>
      <img src={recipe.mainImg}></img>
      <h2>Ingredients</h2>
      <div style={{ marginBottom: "1rem" }}>
        {recipe.ingredients?.map((item) => (
          <IngredientsRow>
            <span>{item.name}</span>
            <span>{item.amount}</span>
          </IngredientsRow>
        ))}
      </div>
      <ImgAds />
      <h2>Instruction</h2>
      {recipe.instruction?.map((item) => (
        <InstructionRow>
          <img src={item.img}></img>
          <p>{item.description}</p>
        </InstructionRow>
      ))}
      <ImgAds />
      <BackArrow
        onClick={() => {
          history.back();
        }}
      >
        <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDE1IDI2IiBoZWlnaHQ9IjI2cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxNSAyNiIgd2lkdGg9IjE1cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxwb2x5Z29uIGZpbGw9IiMyMzFGMjAiIHBvaW50cz0iMTIuODg1LDAuNTggMTQuOTY5LDIuNjY0IDQuMTMzLDEzLjUgMTQuOTY5LDI0LjMzNiAxMi44ODUsMjYuNDIgMi4wNDksMTUuNTg0IC0wLjAzNSwxMy41ICIvPjwvc3ZnPg==" />
      </BackArrow>
    </ReceiptPage>
  );
};

const mapStateToProps = () =>
  createStructuredSelector({
    recipes: getRecipes,
    isLoading: getMainDataLoading,
    error: getError,
  });

export default connect(mapStateToProps, { getMainData: getMainDataAction })(
  Receipt
);
