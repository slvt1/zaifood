import styled, { css } from "styled-components";

export const Catalog = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #749D56;
`;

export const CatalogWrapper = styled.div`
  width: 90vw;
  height: 90vh;
  background-color: #fff;
  border-radius: 32px;
  position: relative;
`;

export const OrnamentImg = styled.img`
  position: absolute;
  left: 0;
  height: 60%;
  top: 20%;
  user-drag: none;
  user-select: none;
  -moz-user-select: none;
  -webkit-user-drag: none;
  -webkit-user-select: none;
  -ms-user-select: none;
`;

export const NavBarWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  padding-left: 4rem;
  padding-right: 4rem;
  margin-top: 3rem;
`;

export const MainTitle = styled.p`
  font-size: 3rem;
  font-weight: bold;
  margin-bottom: 1.5rem;
`;

export const BodyWrapper = styled.p`
  margin-left: 15%;
  /* margin-top: 1rem; */
`;

export const MealList = styled.div``;
