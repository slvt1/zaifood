import styled from "styled-components";

export const CookbookPage = styled.div`
  display: flex;
  flex-direction: row;
  margin: 1rem;
  margin-left: 6rem;
  gap: 1rem;
  position: relative;
`;

export const AdsWrapper = styled.div`
  position: fixed;
  bottom: 1rem;
  right: 1rem;
`;

export const GraphWrapper = styled.div`
margin-top: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
