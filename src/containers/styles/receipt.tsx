import styled from "styled-components";

export const ReceiptPage = styled.div`
  position: relative;
  width: min(600px, 100%);
  margin: 3rem auto;
  h1 {
    margin-bottom: 1rem;
    text-align: center;
  }
  img {
    width: 100%;
  }
  h2 {
    margin-top: 3rem;
    margin-bottom: 1rem;
  }
`;

export const ReceiptInfo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  span {
    margin: 0 0.5rem;
    margin-bottom: 4rem;
  }
`;

export const IngredientsRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  border-bottom: 1px solid ${(props) => "#f7f6f6"};
`;

export const InstructionRow = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  gap: 1rem;
  margin-bottom: 2rem;
`;

export const BackArrow = styled.button`
  position: fixed;
  top: 1rem;
  left: 1rem;
  width: 2rem !important;
  height: 100%;
`;
