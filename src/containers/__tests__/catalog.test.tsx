import React from "react";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import * as Thunk from "redux-thunk";
import moxios from "moxios";
import { act } from "react-dom/test-utils";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import reducer from "@main/__data__/reducers";

import Catalog from "../catalog";
import { LikeButton } from "../../components/MealCard/styled";
import { MealCard } from "../../components/MealCard";
import { Search } from "../../components/Search";
import { MealFilter } from "../../components/MealFilter";

configure({ adapter: new Adapter() });

describe("mount all app", () => {
  beforeEach(() => {
    moxios.install();
    window.__webpack_public_path__ = "/";
  });

  afterEach(() => moxios.uninstall());

  it("mount", async () => {
    const app = mount(
      <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
        <Router basename="/zaifood/">
          <Catalog />
        </Router>
      </Provider>
    );

    expect(app).toMatchSnapshot();

    await moxios.wait(jest.fn);

    await act(async () => {
      const request = moxios.requests.mostRecent();
      await request.respondWith({
        status: 200,
        response: require("../../../stubs/api/data/recipes"),
      });
    });

    app.update();
    expect(app).toMatchSnapshot();
  });

  it("show error on error", async () => {
    const app = mount(
      <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
        <Router basename="/zaifood/">
          <Catalog />
        </Router>
      </Provider>
    );

    await moxios.wait(jest.fn);

    await act(async () => {
      const request = moxios.requests.mostRecent();
      await request.respondWith({
        status: 404,
      });
    });

    app.update();
    expect(app).toMatchSnapshot();
  });

  it("like button liked", () => {
    const app = mount(<LikeButton isLiked={true}></LikeButton>);

    app.update();
    expect(app).toMatchSnapshot();
  });

  it("like button unliked", () => {
    const app = mount(<LikeButton isLiked={false}></LikeButton>);

    app.update();
    expect(app).toMatchSnapshot();
  });

  it("meal card click to like", () => {
    const mealCard = mount(
      <MealCard
        mainImg=""
        name=""
        description=""
        kcal=""
        portion=""
        time=""
        _id=""
      ></MealCard>
    );
    const likeBtn = mealCard.find(".likeBtn").at(0).simulate("click");
    expect(mealCard).toMatchSnapshot();
  });

  it("click to search", () => {
    const search = mount(<Search onChange={() => {}} />);
    search.simulate("click");
    expect(search).toMatchSnapshot();
  });

  it("click to search enter", () => {
    const search = mount(<Search onChange={() => {}} />);
    search.simulate("click");
    search.find("input").simulate("keypress", { key: "Enter" });
    expect(search).toMatchSnapshot();
  });

  it("click to button in filter list", () => {
    const mealFilter = mount(<MealFilter onChange={() => {}} />);
    mealFilter.find("button").at(0).simulate("click");
    expect(mealFilter).toMatchSnapshot();
  });
});
