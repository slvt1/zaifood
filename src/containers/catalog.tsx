import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createStructuredSelector } from "reselect";

import * as stl from "./styles/catalog";
import { Search } from "../components/Search";
import { NavBar } from "../components/NavBar";
import { FilterList } from "../components/FilterList";
import { MealFilter } from "../components/MealFilter";
import { MealList } from "../components/MealList";

import Skeleton from "react-loading-skeleton";

import getMainDataAction from "../__data__/actions/recipes";
import {
  getRecipes,
  getMainDataLoading,
  getError,
} from "@main/__data__/selectors/mainData";

import { bigWhiteBg, echpochmak, kistibiy } from "../assets/index";

const Catalog = ({ getMainData, recipes, isLoading, error }) => {
  React.useEffect(() => {
    getMainData();
  }, []);
  const [filterByText, onFilterChange] = React.useState("");
  const [filterByTime, onTimeFilterChange] = React.useState("All");
  const [filterByIng, onIngreditensChange] = React.useState([]);
  console.log(
    !!recipes
      ? recipes?.result
          ?.filter(
            (item) =>
              item.name.toLowerCase().indexOf(filterByText.toLowerCase()) !== -1
          )
          ?.filter((item) =>
            filterByTime === "All" ? true : item.timeInDay === filterByTime
          )
          ?.filter(
            (item) =>
              filterByIng.length === 0 ||
              item.ingredients.filter(
                (ing) => filterByIng.indexOf(ing.name) !== -1
              ).length !== 0
          )
      : []
  );

  return (
    <stl.Catalog>
      <stl.CatalogWrapper>
        {/* <stl.OrnamentImg alt="ornament" src={bigWhiteBg}></stl.OrnamentImg> */}
        <stl.NavBarWrapper>
          <Search onChange={onFilterChange}></Search>
          <NavBar></NavBar>
        </stl.NavBarWrapper>
        <stl.BodyWrapper>
          <stl.MainTitle>Discover</stl.MainTitle>
          <FilterList
            onChange={(filter) => onTimeFilterChange(filter)}
          ></FilterList>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <MealFilter onChange={onIngreditensChange}></MealFilter>
            {isLoading ? (
              <Skeleton width="280px" height="430px" />
            ) : (
              <MealList
                cards={
                  !!recipes
                    ? recipes?.result
                        ?.filter(
                          (item) =>
                            item.name
                              .toLowerCase()
                              .indexOf(filterByText.toLowerCase()) !== -1
                        )
                        ?.filter((item) =>
                          filterByTime === "All"
                            ? true
                            : item.timeInDay === filterByTime
                        )
                        ?.filter(
                          (item) =>
                            filterByIng.length === 0 ||
                            item.ingredients.filter(
                              (ing) => filterByIng.indexOf(ing.name) !== -1
                            ).length !== 0
                        )
                    : []
                }
              ></MealList>
            )}
          </div>
        </stl.BodyWrapper>
      </stl.CatalogWrapper>
    </stl.Catalog>
  );
};

const mapStateToProps = () =>
  createStructuredSelector({
    recipes: getRecipes,
    isLoading: getMainDataLoading,
    error: getError,
  });

export default connect(mapStateToProps, { getMainData: getMainDataAction })(
  Catalog
);
