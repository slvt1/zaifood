import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createStructuredSelector } from "reselect";
import { useHistory, useLocation } from "react-router-dom";
import Skeleton from "react-loading-skeleton";

import { VideoAds } from "../components/Ads";
import { MealCard } from "../components/MealCard";
import { CookbookPage, AdsWrapper, GraphWrapper } from "./styles/cookbook";
import { BackArrow } from "./styles/receipt";
import { Chart } from "../components/Chart";

import getMainDataAction from "../__data__/actions/recipes";
import {
  getRecipes,
  getMainDataLoading,
  getError,
} from "@main/__data__/selectors/mainData";

import { bigWhiteBg, echpochmak, kistibiy } from "../assets/index";

const Cookbook = ({ getMainData, recipes, isLoading, error }) => {
  const historyCom = useHistory();

  const [filteredRecipes, onLoadRecipes] = React.useState([]);
  const ids = localStorage
    .getItem("liked")
    ?.split("|")
    ?.filter((item) => item !== "");

  React.useEffect(() => {
    getMainData();
  }, []);

  React.useEffect(() => {
    if (!isLoading && !!recipes) {
      onLoadRecipes(
        ids.map((item) => recipes.result?.filter((rec) => rec._id === item)[0])
      );
    }
  }, [recipes]);

  return (
    <>
      <GraphWrapper>
        <h2>Amount of likes</h2>
        <Chart likes={filteredRecipes?.length} />
      </GraphWrapper>
      <CookbookPage>
        {filteredRecipes.map((item) => (
          <MealCard
            {...item}
            onClick={() => {
              historyCom.push(`receipt/${item["_id"]}`);
            }}
          />
        ))}

        <AdsWrapper>
          <VideoAds />
        </AdsWrapper>

        <BackArrow
          onClick={() => {
            history.back();
          }}
        >
          <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDE1IDI2IiBoZWlnaHQ9IjI2cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxNSAyNiIgd2lkdGg9IjE1cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxwb2x5Z29uIGZpbGw9IiMyMzFGMjAiIHBvaW50cz0iMTIuODg1LDAuNTggMTQuOTY5LDIuNjY0IDQuMTMzLDEzLjUgMTQuOTY5LDI0LjMzNiAxMi44ODUsMjYuNDIgMi4wNDksMTUuNTg0IC0wLjAzNSwxMy41ICIvPjwvc3ZnPg==" />
        </BackArrow>
      </CookbookPage>
    </>
  );
};

const mapStateToProps = () =>
  createStructuredSelector({
    recipes: getRecipes,
    isLoading: getMainDataLoading,
    error: getError,
  });

export default connect(mapStateToProps, { getMainData: getMainDataAction })(
  Cookbook
);
