import echpochmak from "./echpechmak.png";
import articleBack from "./article-back3.png";
import bigWhiteBg from "./big-bg.svg";
import kistibiy from "./kistibiy.png";
import leftGreenBg from "./left-green-bg.svg";
import rightGreenBg from "./right-green-bg.svg";
import redMarker from "./red-marker.svg";
import leftWhiteBg from "./left-white-bg.svg";
import rightWhiteBg from "./right-white-bg.svg";
import whiteMarker from "./white-marker.svg";
import searchImg from "./search.svg";
import adsImg from "./1xbet.jpg";
import forcedrop from "./forcedrop.mp4";

export {
  echpochmak,
  articleBack,
  bigWhiteBg,
  kistibiy,
  leftGreenBg,
  rightGreenBg,
  redMarker,
  leftWhiteBg,
  rightWhiteBg,
  whiteMarker,
  searchImg,
  adsImg,
  forcedrop,
};
