export interface AsyncDataState<T, E = string> {
  loading: boolean;
  data?: T;
  error?: E;
}

// Информация по запросу данных для страницы.
export interface RecipesRequest {
  status: Status;
  recipes: Array<Recipe>;
  id: String;
}

export type Recipe = {
  id: String;
  name: String;
  description: String;
  type: String;
  portion: Number;
  time: Number;
  likes: Number;
  kcal: Number;
  mainImg: String;
  instruction: {
    img: String;
    description: String;
  }[];
  ingredients: {
    name: String;
    amount: Number;
  }[];
  timeInDay: String;
};

type Status = {
  code: number;
};
