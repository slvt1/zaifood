import { createSelector } from "reselect";

const getStore = (state) => state.catalog;

const getmainData = createSelector(getStore, (state) => {
  return state?.data;
});

export const getRecipes = createSelector(
  getmainData,
  (state) => state
);

export const getMainDataLoading = createSelector(
  getStore,
  (state) => state?.loading
);
export const getError = createSelector(getStore, (state) => state?.error);
