import {
  RECIPES_FETCH,
  RECIPES_FETCH_SUCCESS,
  RECIPES_FETCH_FAIL,
} from "../../__data__/constants/actions-types";
import { RecipesRequest, AsyncDataState, Recipe } from "../model/interfaces";
import { AnyAction } from "redux";

type RecipesState = Partial<RecipesRequest> & AsyncDataState<Recipe[]>;

const initialState: RecipesState = {
  data: null,
  loading: false,
  error: null,
};

const fetchHandler = (
  state: RecipesState,
  action: AnyAction
): RecipesState => ({
  ...state,
  loading: true,
});

const fetchSuccessHandler = (
  state: RecipesState,
  action: AnyAction
): RecipesState => {
  return {
    ...state,
    data: action.data,
    loading: false,
  };
};

const fetchErrorHandler = (
  state: RecipesState,
  action: AnyAction
): RecipesState => ({
  ...state,
  data: [],
  loading: false,
  error: action.error || true,
});

const handlers = {
  [RECIPES_FETCH]: fetchHandler,
  [RECIPES_FETCH_SUCCESS]: fetchSuccessHandler,
  [RECIPES_FETCH_FAIL]: fetchErrorHandler,
};

export const catalog = (
  state: RecipesState = initialState,
  action: AnyAction
) =>
  Object.prototype.hasOwnProperty.call(handlers, action.type)
    ? handlers[action.type](state, action)
    : state;
