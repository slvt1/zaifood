import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
// import { authHeader } from './__data__/helpers/auth-header';
import { getConfig } from "@ijl/cli";

import { RecipesRequest } from "../model/interfaces";
import {
  RECIPES_FETCH,
  RECIPES_FETCH_SUCCESS,
  RECIPES_FETCH_FAIL,
} from "../constants/actions-types";

const getFetchAction = () => ({
  type: RECIPES_FETCH,
});

const getSuccessAction = (data) => ({
  type: RECIPES_FETCH_SUCCESS,
  data,
});

const getErrorAction = () => ({
  type: RECIPES_FETCH_FAIL,
});

export default () => async (dispatch: any) => {
  dispatch(getFetchAction());

  const requestProps: AxiosRequestConfig = {
    method: "get",
    headers: {
      "Content-Type": "application/json",
    },
  };
  const mainApiBaseUrl = getConfig()["zaifood.api.base.url"];
  try {
    const answer: AxiosResponse<RecipesRequest> = await axios(
      `${mainApiBaseUrl}recipe`,
      requestProps
    );

    dispatch(getSuccessAction(answer.data));
  } catch (error) {
    dispatch(getErrorAction());
  }
};
