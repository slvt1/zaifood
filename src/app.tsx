import * as React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import styled, { css, ThemeProvider } from "styled-components";
import mainTheme from "./theme/mainTheme";
import GlobalStyle from "./theme/globalStyle";
import Catalog from "./containers/catalog";
import Cookbook from "./containers/Cookbook";
import Receipt from "./containers/receipt";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import * as Thunk from "redux-thunk";

import reducer from "./__data__/reducers";

const AppStyle = styled.div`
  /* height: 100%; */
  /* background-color: ${(props) => props.theme.color.primary}; */
`;

export const App: React.FC = () => {
  if (!localStorage.getItem("liked")) {
    localStorage.setItem("liked", "");
  }

  return (
    <Provider
      store={createStore(
        reducer,
        composeWithDevTools(applyMiddleware(Thunk.default))
      )}
    >
      <ThemeProvider theme={mainTheme}>
        <AppStyle>
          <Router basename="/zaifood/">
            <Switch>
              <Route path="/cookbook">
                <Cookbook />
              </Route>
              <Route path="/receipt/:id">
                <Receipt />
              </Route>
              <Route path="/">
                <Catalog />
              </Route>
            </Switch>
          </Router>
        </AppStyle>
      </ThemeProvider>
    </Provider>
  );
};
