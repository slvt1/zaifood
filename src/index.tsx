import * as React from "react";
import * as ReactDOM from "react-dom";
import GlobalStyle from "./theme/globalStyle";
import { App } from "./app";

export default () => (
  <>
    <App />
    <GlobalStyle />
  </>
);

export const mount = (Component: any) => {
  ReactDOM.render(<Component />, document.getElementById("app"));
};

export const unmount = () => {
  ReactDOM.unmountComponentAtNode(document.getElementById("app"));
};
