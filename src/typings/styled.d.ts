// import original module declarations
import "styled-components";

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme {
    color: {
      primary: string;
      secondary: string;
      white: string;
      gray1: string;
      gray2: string;
      red: string;
    };
  }
}
