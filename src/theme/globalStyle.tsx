import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  @import url("https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap");

  * {
    box-sizing: border-box;
    font-family: 'Montserrat', sans-serif;
  }

  body {
    padding: 0;
    margin: 0;
    font: 16px Montserrat, sans-serif;
  }

  h1, h2, h3, p, li {
    font-family: 'Montserrat', sans-serif;
    margin: 0;
  }

  button {
    cursor: pointer;
    border: 0;
    padding: 0;
    font-family: '', sans-serif;
    background-color: transparent;
  }

  ol {
    padding: 0;
    margin: 0;
  }
`;

export default GlobalStyle;
