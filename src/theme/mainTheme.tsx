import { DefaultTheme } from "styled-components";

const theme: DefaultTheme = {
  color: {
    primary: "#749D56",
    secondary: "#F1FBEF",
    white: "#fff",
    gray1: "#929292",
    gray2: "#f7f6f6",
    red: "#E69B94",
  },
};

export default theme;
