import styled from "styled-components";

export const NavBarStyled = styled.div`
  display: flex;
  flex-direction: row;
  gap: 42px;
  align-items: center;
`;
