import * as React from "react";
import { NavBarStyled } from "./styled";
import { Button } from "../Button";
import { Link } from "react-router-dom";
import { Modal } from "../Modals/Modal";
import { Login } from "../Modals/Login/Login";

export const NavBar: React.FC = () => {
  return (
    <NavBarStyled>
      <span style={{ fontWeight: "bold" }}>
        <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
          Discover
        </Link>
      </span>
      <span>
        <Link
          to="/cookbook"
          style={{ textDecoration: "none", color: "inherit" }}
        >
          My Cookbook
        </Link>
      </span>
      {/* {isLogined ? (
        <Button
          typeBtn="primary"
          onClick={() => {
            onLogined(false);
            localStorage.setItem("login", "");
            localStorage.setItem("password", "");
            localStorage.setItem("token", "");
          }}
        >
          Logout
        </Button>
      ) : (
        <Button typeBtn="primary" onClick={() => onLoginOpen(true)}>
          Login
        </Button>
      )} */}
      {/* <Modal isOpen={isLoginOpen} onCloseModal={() => onLoginOpen(false)}>
        <Login
          isLogin={true}
          onSubmit={(e) => {
            if (
              !!localStorage.getItem("login") &&
              localStorage.getItem("login") !== ""
            ) {
              if (
                localStorage.getItem("login") === e.login &&
                localStorage.getItem("password") === e.password
              ) {
                localStorage.setItem("token", "token");
                onLogined(true);
              }
            } else {
              localStorage.setItem("login", e.login);
              localStorage.setItem("password", e.password);
              localStorage.setItem("token", "token");
              onLogined(true);
            }
            onLoginOpen(false);
          }}
        ></Login>
      </Modal> */}
    </NavBarStyled>
  );
};
