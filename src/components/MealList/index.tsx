import * as React from "react";
import { MealListStyled } from "./styled";
import { useHistory, useLocation } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { MealCardProps, MealCard } from "../MealCard";

type MealListType = { cards: MealCardProps[] };

export const MealList: React.FC<MealListType> = (props) => {
  const elementsToShow = props.cards?.length === 1 ? 1 : 2;
  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: elementsToShow,
    slidesToScroll: elementsToShow,
  };
  const history = useHistory();
  return (
    <MealListStyled>
      <Slider {...settings} style={{ width: 600 }}>
        {props.cards?.map((item) => (
          <MealCard
            key={item.name}
            {...item}
            onClick={() => {
              history.push(`receipt/${item["_id"]}`);
            }}
          />
        ))}
      </Slider>
    </MealListStyled>
  );
};
