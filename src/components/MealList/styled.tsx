import styled from "styled-components";

export const MealListStyled = styled.div`
  display: flex;
  flex-direction: row;
  gap: 3rem;
`;
