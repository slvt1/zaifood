import styled from "styled-components";

export const ButtonStyled = styled.button<{ typeBtn: string }>`
  color: ${(props) => (props.typeBtn === "primary" ? "#749D56" : "#929292")};
  background-color: ${(props) =>
    props.typeBtn === "primary" ? "#F1FBEF" : "#f7f6f6"};
  padding: 0.5rem 1rem;
  border-radius: 1rem;
  font-size: 1rem;
  justify-content: center;
  &:focus {
    outline: none;
  }
`;
