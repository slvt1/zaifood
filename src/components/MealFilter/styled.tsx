import styled from "styled-components";

export const MealFilterStyle = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-auto-rows: auto;
  /* margin-top: 20%; */
  width: 15rem;
  gap: 0.5rem;
  padding: 4rem 0;
  margin-right: 2rem;
`;
