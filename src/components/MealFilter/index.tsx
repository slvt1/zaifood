import * as React from "react";
import { MealFilterStyle } from "./styled";

import { Button } from "../Button";

type MealFilter = {
  onChange: any;
};

export const MealFilter: React.FC<MealFilter> = (props) => {
  let data = [
    "Beef",
    "Salt",
    "Vegetable oil",
    "Meat",
    "Potatoes",
    "Onion",
    "Mutton",
    "Honey",
    "Garlic",
    "Wheat flour",
  ];

  const [active, onActive] = React.useState([]);

  React.useEffect(() => {
    props.onChange(active);
  }, [active]);

  return (
    <MealFilterStyle>
      {data?.map((item) => (
        <Button
          typeBtn={active.indexOf(item) !== -1 ? "primary" : "secondary"}
          style={{ display: "flex", alignItems: "center" }}
          onClick={() => {
            active.indexOf(item) !== -1
              ? onActive(active?.filter((btn) => btn !== item))
              : onActive([...active, item]);
          }}
        >
          {item}
        </Button>
      ))}
    </MealFilterStyle>
  );
};
