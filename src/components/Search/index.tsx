import * as React from "react";
import { SearchStyle } from "./styled";
import { searchImg } from "../../assets";
import { Formik, Form, Field } from "formik";

type SearchProps = { onChange: any };

export const Search: React.FC<SearchProps> = (props) => {
  const [clicked, onClick] = React.useState(false);
  const inputBtnRef = React.useRef(null);

  return (
    <SearchStyle
      onClick={() => {
        onClick(true);
      }}
    >
      {clicked ? (
        <Formik
          initialValues={{ searchText: "" }}
          onSubmit={(e) => props.onChange(e.searchText)}
        >
          <Form onChange={() => inputBtnRef.current.click()}>
            <Field
              name="searchText"
              id="searchText"
              onBlur={() => onClick(false)}
            ></Field>
            <button
              style={{ display: "none" }}
              type="submit"
              ref={inputBtnRef}
            ></button>
          </Form>
        </Formik>
      ) : (
        <img src={searchImg} />
      )}
    </SearchStyle>
  );
};
