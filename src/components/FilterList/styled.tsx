import styled from "styled-components";

export const FilterListStyle = styled.div`
  display: flex;
  flex-direction: row;
  /* margin-bottom: 5vh; */
`;

export const FilterListItem = styled.div<{ isMain: boolean }>`
  color: #749d56;
  font-weight: ${(props) => (props.isMain ? "bold" : "normal")};
  font-size: 1.5rem;
  margin-right: 1.5rem;
  cursor: pointer;
`;
