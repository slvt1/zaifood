import * as React from "react";
import { FilterListStyle, FilterListItem } from "./styled";

type FilterListProps = {
  onChange?: any;
};

export const FilterList: React.FC<FilterListProps> = (props) => {
  const [active, onActive] = React.useState("All");
  const items = ["All", "Breakfast", "Launch", "Dinner"];

  React.useEffect(() => {
    props.onChange(active);
  }, [active]);

  return (
    <FilterListStyle>
      {items?.map((item) => (
        <FilterListItem
          isMain={active === item}
          onClick={() => {
            onActive(item);
          }}
        >
          {item}
        </FilterListItem>
      ))}
    </FilterListStyle>
  );
};
