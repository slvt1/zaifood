import * as React from "react";
import { default as ReactModal } from "react-modal";

type ModalProps = {
  isOpen: boolean;
  onCloseModal: any;
};

const modalStyle = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

export const Modal: React.FC<ModalProps> = (props) => {
  return (
    <ReactModal
      style={modalStyle}
      isOpen={props.isOpen}
      onRequestClose={props.onCloseModal}
    >
      {props.children}
    </ReactModal>
  );
};
