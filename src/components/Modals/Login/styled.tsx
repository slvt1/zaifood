import styled from "styled-components";

export const LoginForm = styled.div`
  form {
    display: flex;
    flex-direction: column;
  }
  .error-message{
    font-size: 10px;
    color: red;
  }
  button{
    margin-top: 1rem;
  }
  input{
    margin-bottom: 1rem;
  }
`;
