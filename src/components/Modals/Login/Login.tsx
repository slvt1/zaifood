import * as React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { LoginForm } from "./styled";

export const LoginSchema = Yup.object().shape({
  login: Yup.string().required("Required"),
  password: Yup.string()
    .required("No password provided.")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(
      /[a-zA-Z0-9]/,
      "Password can only contain Latin letters and numbers."
    ),
});

type LoginProps = {
  isLogin: boolean;
  onSubmit?: any;
};

export const Login: React.FC<LoginProps> = (props) => {
  return (
    <LoginForm>
      <Formik
        initialValues={{
          login: "",
          password: "",
        }}
        validationSchema={LoginSchema}
        onSubmit={props.onSubmit}
      >
        {({ errors, touched }) => (
          <Form>
            <label>Login</label>
            <Field id="emailInput" name="login" />
            {/* {errors.login && touched.login ? (
              <div className="error-message">{errors.login}</div>
            ) : null} */}
            <label>Password</label>
            <Field id="passwordInput" type="password" name="password"></Field>
            {/* {errors.password && touched.password ? (
              <div className="error-message">{errors.password}</div>
            ) : null} */}
            <button className="primary-btn primary-btn_bg" type="submit">
              "Log in"
            </button>
          </Form>
        )}
      </Formik>
    </LoginForm>
  );
};
