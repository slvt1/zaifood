import styled from "styled-components";

export const MealCardStyle = styled.div`
  width: 285px;
  background-color: #F1FBEF;
  padding: 2rem;
  position: relative;
  border-radius: 3rem;
  cursor: pointer;
`;

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 0.5rem;
  margin-bottom: 2rem;
  color: #749d56;
`;

export const Img = styled.img`
  width: 100%;
  margin-bottom: 1rem;
  max-height: 150px;
`;

export const Title = styled.h3`
  font-weight: bold;
  margin-bottom: 0.5rem;
`;

export const Desc = styled.p`
  font-size: 14px;
  color: #749d56;
`;

export const LikeButton = styled.div<{ isLiked: boolean }>`
  position: absolute;
  bottom: 1rem;
  right: 1rem;
  background-color: ${(props) => (props.isLiked ? "#E69B94" : "#f7f6f6")};
  width: 2rem;
  height: 2rem;
  border-radius: 1rem;
`;
