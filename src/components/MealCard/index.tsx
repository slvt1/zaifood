import * as React from "react";
import {
  MealCardStyle,
  InfoWrapper,
  Title,
  Desc,
  Img,
  LikeButton,
} from "./styled";

export type MealCardProps = {
  mainImg: string;
  name: string;
  description: string;
  kcal: string;
  portion: string;
  time: string;
  _id: string;
};

export const MealCard: React.FC<
  MealCardProps & React.HTMLProps<HTMLDivElement>
> = (props) => {
  const [isLiked, onLike] = React.useState(
    localStorage.getItem("liked")?.split("|").indexOf(props._id) !== -1
  );
  return (
    <MealCardStyle onClick={props.onClick}>
      <Img src={props.mainImg} alt="meal card img" />
      <InfoWrapper>
        <p>
          <span style={{ fontWeight: "bold" }}>{props.kcal}</span>
          <span> kcal</span>
        </p>
        <p>
          <span style={{ fontWeight: "bold" }}>{props.portion}</span>
          <span> people</span>
        </p>
        <p>
          <span style={{ fontWeight: "bold" }}>{props.time}</span>
          <span> min</span>
        </p>
      </InfoWrapper>
      <Title>{props.name}</Title>
      <Desc>{props.description}</Desc>
      <LikeButton
        className="likeBtn"
        isLiked={isLiked}
        onClick={(e) => {
          const likedItems = localStorage.getItem("liked")?.split("|");
          if (isLiked) {
            likedItems?.indexOf(props._id) !== -1 &&
              localStorage.setItem(
                "liked",
                likedItems?.filter((item) => item !== props._id).join("|")
              );
          } else {
            likedItems?.indexOf(props._id) === -1 &&
              localStorage.setItem(
                "liked",
                [...likedItems, props._id].join("|")
              );
          }
          onLike(!isLiked);
          e.stopPropagation();
        }}
      ></LikeButton>
    </MealCardStyle>
  );
};
