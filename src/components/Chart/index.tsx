import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import React from "react";

type ChartProps = {
  likes: number;
};

export const Chart: React.FC<ChartProps> = (props) => {
  return (
    <BarChart
      width={500}
      height={300}
      data={[
        {
          name: "now",
          likes: props.likes,
        },
      ]}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="likes" fill="#8884d8" />
    </BarChart>
  );
};
