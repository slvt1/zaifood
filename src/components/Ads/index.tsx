import React from "react";
import ReactPlayer from "react-player";
import { adsImg, forcedrop } from "../../assets";
import styled from "styled-components";

export const adsLink = "https://bit.ly/30rgNlA";

const VideoStyled = styled.div`
  position: relative;

  .exit {
    position: absolute;
    top: -12px;
    left: -7px;
    width: 25px;
    height: 25px;
    cursor: pointer;
  }
`;

export const ImgAds: React.FC = () => {
  return (
    <a href={adsLink} target="_blank">
      <img src={adsImg}></img>
    </a>
  );
};

export const VideoAds: React.FC = () => {
  const [isVisible, onExit] = React.useState(true);
  return (
    <VideoStyled>
      <div
        className="exit"
        style={{ display: !isVisible && "none" }}
        onClick={(e) => {
          onExit(false);
          e.stopPropagation();
        }}
      >❌</div>
      <a
        href={adsLink}
        target="_blank"
        style={{ display: !isVisible && "none" }}
      >
        <video width="560" height="315" autoPlay loop>
          <source src={forcedrop} type="video/mp4" />
        </video>
      </a>
    </VideoStyled>
  );
};
