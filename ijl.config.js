const pkg = require("./package.json");

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
    },
    module: {
      rules: [
        { parser: { system: false } },
        {
          test: /\.tsx?$/,
          loader: "awesome-typescript-loader",
        },
        {
          test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3|mp4)$/,
          loader: "file-loader",
        },
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
      ],
    },
  },
  config: {
    "main.api.base.url": "https://rocky-citadel-84217.herokuapp.com/api/",
    "zaifood.api.base.url": "https://rocky-citadel-84217.herokuapp.com/api/",
  },
  apps: {
    main: { name: "main", version: process.env.VERSION || pkg.version },
  },
  navigations: {
    repos: "/zaifood",
  },
};
