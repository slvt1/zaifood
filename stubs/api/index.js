const fs = require("fs");
const path = require("path");

const router = require("express").Router();

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const loadJson = (filepath, encoding = "utf8") =>
  JSON.parse(
    fs.readFileSync(path.resolve(__dirname, `./data/${filepath}.json`), {
      encoding,
    })
  );

router.get("/recipe", (req, res) => {
  setTimeout(() => {
    res.send(loadJson("./recipes"));
  }, 1000);
});

router.get("/recipe:id", (req, res) => {
  setTimeout(() => {
    res.send(loadJson("./recipe"));
  }, 1000);
});

router.post("/login", (req, res) => {
  setTimeout(() => {
    res.send(loadJson("./getUser"));
  }, 1000);
});

router.get("/likes", (req, res) => {
  setTimeout(() => {
    res.send(loadJson("./recipes"));
  }, 1000);
});

module.exports = router;
